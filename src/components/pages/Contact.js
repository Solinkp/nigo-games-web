import React, { useState } from 'react';
import { Container, Card, CardContent } from '@material-ui/core';
import Reaptcha from 'reaptcha';
import FormInput from '../partials/FormInput';
import FormButton from '../partials/FormButton';
import MessageModal from '../partials/MessageModal';
import { CustPink, CustGrey } from '../settings/colors';

const Contact = () => {
  const templateId = 'nigo_contact';
  const serviceId = 'gmail';
  // const userId = 'user_Lq3XfOynN9i8hwwIAoGax'; // kilux id
  const userId = 'user_7rHmZMcuROgUHoTA672eS';
  const reKey = '6LdaXdQUAAAAAFQ50mdEnEZZcZOTYT6bZOqUOgud';
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [disable, setDisable] = useState(true);
  const [open, setOpen] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [modalBody, setModalBody] = useState('');
  const windowWidth = window.innerWidth;
  const textAreaRows = windowWidth < 720 ? '7' : '12';
  const fontSize = windowWidth < 720 ? 14 : 20;
  let captcha = null;
  window.emailjs.init(userId);

  const onInputText = (e) => {
    e.target.setCustomValidity('');
  }

  const onInvalidInput = (e, text) => {
    e.target.setCustomValidity(text);
  };

  const cleanForm = () => {
    setName('');
    setEmail('');
    setMessage('');
    setDisable(true);
  }

  const sendMail = (e) => {
    e.preventDefault();
    if(name === '' || email === '' || message === '') {
      return;
    }
    setDisable(true);
    const params = {
      name: name,
      from_email: email,
      message: message
    }
    captcha.reset();
    window.emailjs.send(serviceId, templateId, params, userId).then(function(_) {
      cleanForm();
      setModalTitle('Correo Enviado');
      setModalBody('Gracias por contactarnos, te responderemos lo más pronto posible.');
      setOpen(true);
    }, function(_) {
      cleanForm();
      setModalTitle('Oops');
      setModalBody('Lo sentimos, hubo un problema, intenta de nuevo más tarde.');
      setOpen(true);
    });
  }

  return (
    <Container maxWidth="sm">
      <center><h1>CONTACTO</h1></center>
      <Card className="contact-card">
        <CardContent style={{padding: 0}}>
          <form onSubmit={sendMail}>
            <FormInput
              id = "name"
              invalidText = {(e) => onInvalidInput(e, "Por favor, llena este campo.")}
              onInputText = {(e) => onInputText(e)}
              label = "Nombre"
              required = {true}
              type = "text"
              value = {name}
              length = "40"
              fontSize = {fontSize}
              onChange = {(e) => setName(e.target.value)}
            />

            <FormInput
              id = "email"
              invalidText = {(e) => onInvalidInput(e, "Por favor, llena este campo correctamente.")}
              onInputText = {(e) => onInputText(e)}
              label = "Correo electrónico"
              required = {true}
              type = "email"
              value = {email}
              length = "40"
              fontSize = {fontSize}
              onChange = {(e) => setEmail(e.target.value)}
            />

            <FormInput
              id = "message"
              invalidText = {(e) => onInvalidInput(e, "Por favor, llena este campo.")}
              onInputText = {(e) => onInputText(e)}
              label = "Mensaje"
              required = {true}
              type = "text"
              value = {message}
              onChange = {(e) => setMessage(e.target.value)}
              multiline
              rows = {textAreaRows}
              fontSize = {fontSize}
              lineHeight = {1}
            />

            <Container className="contact-form__input">
              <Reaptcha
                ref={e => (captcha = e)}
                sitekey = {reKey}
                onVerify = {() => setDisable(false)}
                onExpire = {() => setDisable(true)}
                theme = "dark"
              />
            </Container>

            <FormButton
              disabled = {disable}
              fullWidth = {true}
              variant = "contained"
              size = "large"
              title = "ENVIAR"
              type = "submit"
              colorBtn = { disable ? CustGrey : CustPink }
              colorBtnTxt = '#fff'
            />
          </form>
        </CardContent>
      </Card>

      <MessageModal
        open = {open}
        onClose = {() => setOpen(false)}
        title = {modalTitle}
        body = {modalBody}
      />
    </Container>
  );
};

export default Contact;