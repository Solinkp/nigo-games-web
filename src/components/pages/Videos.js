import React, { useState, useEffect } from 'react';
import axios from "axios";
import YouTube from 'react-youtube';
import { Container, Grid, Divider } from '@material-ui/core';
import NormButton from '../partials/NormButton';
import { CustPink } from '../settings/colors';

const Videos = () => {
  const channelId = "UCHSEOo1biNLYkGpB7BFaEXA";
  const apiKey = "AIzaSyD5-rG3HlJfKbvU75no4iA8hbNLnsK9t6s"
  const [videos, setVideos] = useState([]);
  const opts = {
    height: '250',
    width: '100%',
    playerVars: {
      origin: 'https://www.youtube.com',
    }
  }

  useEffect(() => {
    axios
      .get("https://www.googleapis.com/youtube/v3/search?key="+apiKey+"&channelId="+channelId+"&part=snippet,id&order=date&maxResults=9")
      .then(({ data }) => {
        if (data.items[data.items.length-1].id.channelId) {
          data.items.pop();
        }
        const items = [];
        data.items.forEach(item => {
          items.push({videoTitle: item.snippet.title, videoId: item.id.videoId});
        });
        setVideos(items);
      });
  }, []);

  const goToYt = () => {
    window.open('https://www.youtube.com/channel/'+channelId+'/videos', "_blank");
  }

  return (
    <Container maxWidth="lg">
      <center><h1>VIDEOS RECIENTES</h1></center>
      <Grid container={true} spacing={3}>
        {
          videos.map((video, index) => (
            <Grid key={index} item xs={12} sm={4}>
              <YouTube
                videoId={video.videoId}
                opts={opts}
              />
            </Grid>
          ))
        }
      </Grid>
      <Divider style={{marginTop: 10, marginBottom: 10}}/>
      <NormButton
        title="Visitar Canal"
        fullWidth={true}
        size="large"
        variant="contained"
        fontSize={14}
        colorBtn={CustPink}
        colorBtnTxt="#fff"
        onClick={() => goToYt()}
      />
    </Container>
  );
};

export default Videos;