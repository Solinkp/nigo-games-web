import React from 'react';
import { Container, Grid } from '@material-ui/core';
import HomePaper from '../partials/home/HomePaper';
import HomeCard from '../partials/home/HomeCard';

const Home = () => {
  const gamingText = "NIGO GAMES reúne todo lo que un gamer busca para su diversión y desarrollo; ordenadores gaming de última generación, la mejor conexión a internet y todo lo que requieran los profesionales.";
  const compText = [
    {text: 'En NIGO GAMES encontrarás un lugar único donde desarrollar tus habilidades y destrezas como gamer.', image: '/images/pictures/pc_back.jpg'},
    {text: 'NIGO GAMES es un sitio donde puedes disputar y vivir las competencias como nunca antes la habías experimentado.', image: '/images/pictures/nigo_local_1.jpg'},
    {text: 'En NIGO GAMES promovemos oportunidades para la profesionalización de los gamers.', image: '/images/pictures/nigo_local_2.jpg'},
    {text: 'En nuestra arena se capacitan y entrenan los mejores jugadores para que participen como equipo en competencias internacionales.', image: '/images/pictures/nigo_local_3.jpg'}
  ];
  const esportText = "Los deportes electrónicos son el centro de nuestro modelo de negocio, si buscas vivir experiencias únicas, sensaciones nunca antes vividas, sentimientos que no sabías que existían, NIGO GAMES es tu centro de esports.";
  const esportImages = [
    {src: '/images/pictures/fortnite_back.jpg', alt: 'NG_esp_1'},
    {src: '/images/pictures/lol_back.jpg', alt: 'NG_esp_2'},
    {src: '/images/pictures/smash_back.jpg', alt: 'NG_esp_3'}
  ];

  return (
    <div>
      <div className="home-header">
        <Container className="home-header-content" maxWidth="xl">
          <HomePaper text={gamingText} className="content__paper--parallax" square={true}/>
        </Container>
      </div>

      <Container className="home-compet" maxWidth="xl">
        <div className="home-content">
          <Grid container={true} spacing={4}>
            {
              compText.map((comp, index) => (
                <Grid key={index} item xs={12} sm={6} md={6} lg={3} className="home-content__grid-item">
                  <HomeCard text={comp.text} image={comp.image}/>
                </Grid>
              ))
            }
          </Grid>
        </div>
      </Container>

      <Container maxWidth="xl">
        <center><h1>ESPORTS</h1></center>
        <div className="home-content">
          <Grid container={true} spacing={4}>
            <Grid item xs={12} sm={12} className="home-content__grid-item">
              <HomePaper text={esportText} className="content__paper--normal"/>
            </Grid>
            {
              esportImages.map((image, index) => (
                <Grid key={index} item xs={12} sm={4} className="home-content__grid-item">
                  <img className="grid-item__image bordered-card" src={image.src} alt={image.alt}></img>
                </Grid>
              ))
            }
          </Grid>
        </div>
      </Container>
    </div>
  );
};

export default Home;