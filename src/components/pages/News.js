import React from 'react';
import { Container, Grid } from '@material-ui/core';
import NewsCard from '../partials/NewsCard';

const News = () => {
  const newsTemplate = [
    {
      id: 'n1',
      title: 'Evento de apertura',
      small_description: 'Lorem ipsum asdbjasbdhjsbadhbas dasbdjhbashdjba ashdbhasbdjhbasjhdbsa habsdj',
      body: 'Lorem dasbdjhasd sad sad asdsaasd as d as das d as dasdasdas d',
      author: 'Jerson Castañeda',
      date: '19 de Enero de 2020',
      image: '/images/pictures/fortnite_back.jpg',
      photo_gallery: ''
    },
    {
      id: 'n2',
      title: 'Evento de apertura',
      small_description: 'Lorem ipsum asdbjasbdhjsbadhbas dasbdjhbashdjba ashdbhasbdjhbasjhdbsa habsdj',
      body: 'Lorem dasbdjhasd sad sad asdsaasd as d as das d as dasdasdas d',
      author: 'Jerson Castañeda',
      date: '19 de Enero de 2020',
      image: '/images/pictures/lol_back.jpg',
      photo_gallery: ''
    },
    {
      id: 'n3',
      title: 'Evento de apertura',
      small_description: 'Lorem ipsum asdbjasbdhjsbadhbas dasbdjhbashdjba ashdbhasbdjhbasjhdbsa habsdj',
      body: 'Lorem dasbdjhasd sad sad asdsaasd as d as das d as dasdasdas d',
      author: 'Jerson Castañeda',
      date: '19 de Enero de 2020',
      image: '/images/pictures/pc_back.jpg',
      photo_gallery: ''
    },
    {
      id: 'n4',
      title: 'Evento de apertura',
      small_description: 'Lorem ipsum asdbjasbdhjsbadhbas dasbdjhbashdjba ashdbhasbdjhbasjhdbsa habsdj',
      body: 'Lorem dasbdjhasd sad sad asdsaasd as d as das d as dasdasdas d',
      author: 'Jerson Castañeda',
      date: '19 de Enero de 2020',
      image: '/images/pictures/pubg_back.jpg',
      photo_gallery: ''
    },
    {
      id: 'n5',
      title: 'Evento de apertura',
      small_description: 'Lorem ipsum asdbjasbdhjsbadhbas dasbdjhbashdjba ashdbhasbdjhbasjhdbsa habsdj',
      body: 'Lorem dasbdjhasd sad sad asdsaasd as d as das d as dasdasdas d',
      author: 'Jerson Castañeda',
      date: '19 de Enero de 2020',
      image: '/images/pictures/ow_back.jpg',
      photo_gallery: ''
    }
  ]

  return (
    <Container maxWidth="xl">
      <center><h1>NOTICIAS Y NOVEDADES</h1></center>
      <Grid container={true} spacing={3}>
        {
          newsTemplate.map((news, _) => (
            <NewsCard
              key={news.id}
              image={news.image}
              title={news.title}
              date={news.date}
              small_description={news.small_description}
            />
          ))
        }
      </Grid>
    </Container>
  );
};

export default News;