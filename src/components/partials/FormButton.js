import React from 'react';
import { Container, Button } from '@material-ui/core';

const FormButton = (props) => {
  const backColor = props.colorBtn;
  const foreColor = props.colorBtnTxt;

  return (
    <Container className="contact-form__input">
      <Button
        disabled = {props.disabled}
        fullWidth = {props.fullWidth}
        onClick = {props.onClick}
        variant = {props.variant}
        size = {props.size}
        style = {{
          fontSize: 14,
          backgroundColor: backColor,
          color: foreColor
        }}
        type = {props.type}
      >
        {props.title}
      </Button>
    </Container>
  );
};

export default FormButton;