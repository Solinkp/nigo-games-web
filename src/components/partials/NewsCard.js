import React from 'react';
import { Grid, Card, CardActionArea, CardMedia, CardContent, CardActions } from '@material-ui/core';
import NormButton from '../partials/NormButton';
import { CustPink} from '../settings/colors';

const NewsCard = (props) => (
  <Grid item xs={12} sm={6} md={4} lg={3}>
    <Card className="bordered-card">
      <CardActionArea>
        <CardMedia
          component="img"
          image={props.image}
          title={props.title}
        />
        <CardContent>
          <h2 className="news-card__title">{props.title}</h2>
          <span className="span__subtitle">{props.date}</span>
          <p className="news-card__text">{props.small_description}</p>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <NormButton
          variant = "outlined"
          size = "medium"
          title = "Compartir"
          colorBtn = {CustPink}
          colorBtnTxt = "#fff"
          fontSize = {12}
        />
      </CardActions>
    </Card>
  </Grid>
);

export default NewsCard;