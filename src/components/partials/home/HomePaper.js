import React from 'react';
import { Paper } from '@material-ui/core';

const HomePaper = (props) => (
  <Paper className={"content__paper " + props.className} elevation={3} square={props.square}>
    <h4 className="content__paper-text" align="justify">
      {props.text}
    </h4>
  </Paper>
);

export default HomePaper;