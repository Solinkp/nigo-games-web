import React, { useState } from 'react';
import { Card, CardContent, CardActionArea, CardMedia } from '@material-ui/core';

const HomeCard = (props) => {
  const [flipped, setFlipped] = useState(false);

  return (
    <Card className="bordered-card bordered-card--home-card">
      <CardActionArea className="home-card-content" onClick={() => setFlipped(!flipped)}>
        <CardContent style={{padding: flipped ? 0 : 10}}>
          {
            !flipped && <span className="home-card-content__text">{props.text}</span>
          }
          {
            flipped &&
            <CardMedia
              component="img"
              image={props.image}
            />
          }
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default HomeCard;