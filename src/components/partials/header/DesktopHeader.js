import React from 'react';
import { Container, Grid } from '@material-ui/core';
import LogoHeader from './LogoHeader';
import LoginHeader from './LoginHeader';

const DesktopHeader = () => (
  <Container maxWidth="xl" className="header-image">
    <Grid container={true}>
      <LogoHeader sm={3} />
      <Grid container={true} item sm={6} lg={8} className="header-image-banner"></Grid>
      <LoginHeader size="large" fontSize={16} sm={3}/>
    </Grid>
  </Container>
);

export default DesktopHeader;