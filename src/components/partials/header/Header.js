import React from 'react';
import { Container, Paper } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import MobileHeader from './MobileHeader';
import DesktopHeader from './DesktopHeader';
import { CustDarkGrey } from '../../settings/colors';

const Header = () => {
  const menuList = [
    {name: 'Inicio', route: '/'},
    // {name: 'Torneos', route: '/tournaments'},
    // {name: 'Noticias', route: '/news'},
    {name: 'Videos', route: '/videos'},
    {name: 'Contacto', route: '/contact'}
  ];

  return (
    <header className="header">
      <div className="hide-for-desktop">
        <MobileHeader menuList={menuList}/>
      </div>

      <div className="hide-for-mobile">
        <DesktopHeader />
        <Paper square elevation={5} style={{backgroundColor: CustDarkGrey}} className="header-menu">
          <Container maxWidth="lg" className="header-menu-content">
            {menuList.map((map, index) => (
              <NavLink
                key={index}
                to={map.route}
                exact={index === 0 ? true : false}
                className="header-menu__item"
                activeClassName="is-active"
              >{map.name}</NavLink>
            ))}
          </Container>
        </Paper>
      </div>
    </header>
  );
};

export default Header;