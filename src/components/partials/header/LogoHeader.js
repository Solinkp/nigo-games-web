import React from 'react';
import { Link } from 'react-router-dom';
import { Grid } from '@material-ui/core';

const LogoHeader = (props) => (
  <Grid container={true} item xs={4} sm={props.sm} lg={2} alignContent="center" justify="center">
    <Link to="/"><img className="header-image-logo" src="/images/nigo_games_logo.png" alt="logo"/></Link>
  </Grid>
);
export default LogoHeader;