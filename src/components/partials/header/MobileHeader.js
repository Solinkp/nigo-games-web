import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Button, Icon, List, SwipeableDrawer, Grid, Divider } from '@material-ui/core';
import DrawerListItem from '../DrawerListItem';
import LogoHeader from './LogoHeader';
import LoginHeader from './LoginHeader';
import { CustOrange } from '../../settings/colors';

const MobileHeader = (props) => {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [active, setActive] = useState();
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  const menuList = props.menuList;

  useEffect(() => {
    for (let index = 0; index < menuList.length; index++) {
      if (history.location.pathname === menuList[index].route) {
        setActive(index);
        return;
      }
    }
  }, [history.location.pathname, menuList]);

  const goTo = (index) => {
    history.push(menuList[index].route);
    setOpen(false);
  }

  return (
    <Container className="header-image">
      <Grid container={true}>
        <Grid container={true} item xs={4}>
          <SwipeableDrawer disableBackdropTransition={!iOS} disableDiscovery={iOS} open={open} onOpen={() => setOpen(true)} onClose={() => setOpen(false)}>
            <List style={{marginRight: 20}}>
              {menuList.map((map, index) => (
                <div key={index}>
                  <DrawerListItem index={index} active={active} text={map.name} clickListItem={() => goTo(index)}/>
                  <Divider style={{backgroundColor: CustOrange}}/>
                </div>
              ))}
            </List>
          </SwipeableDrawer>
          <Button>
            <Icon
              className="menu-icon"
              style={{fontSize: 30}}
              onClick={() => setOpen(true)}
            >menu</Icon>
          </Button>
        </Grid>
        <LogoHeader sm={4} />
        <LoginHeader size="small" fontSize={12} sm={4}/>
      </Grid>
    </Container>
  );
};

export default MobileHeader;