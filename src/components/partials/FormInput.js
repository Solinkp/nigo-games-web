import React from 'react';
import { Container, TextField } from '@material-ui/core';

const FormInput = (props) => {
  const length = props.length;
  const lineHeight = props.lineHeight;
  const fontSize = props.fontSize;

  return (
    <Container className="contact-form__input">
      <TextField
        id = {props.id}
        onInvalid = {props.invalidText}
        onInput = {props.onInputText}
        className = "text-field"
        fullWidth
        multiline = {props.multiline}
        rows = {props.rows}
        label = {props.label}
        variant = "outlined"
        required = {props.required}
        type = {props.type}
        value = {props.value}
        inputProps = {{
          maxLength: length,
          style: {
            color: '#fff',
            fontSize: fontSize,
            lineHeight: lineHeight
          }
        }}
        InputLabelProps = {{
          style: {fontSize: fontSize, color: '#fff'}
        }}
        onChange = {props.onChange}
      />
    </Container>
  );
};

export default FormInput;