import React from 'react';
import { Button } from '@material-ui/core';

const NormButton = (props) => {
  const backColor = props.colorBtn;
  const foreColor = props.colorBtnTxt;
  const fontSize = props.fontSize;

  return (
    <Button
      disabled = {props.disabled}
      fullWidth = {props.fullWidth}
      onClick = {props.onClick}
      variant = {props.variant}
      size = {props.size}
      style = {{
        fontSize: fontSize,
        backgroundColor: backColor,
        color: foreColor
      }}
      type = "button"
    >
      {props.title}
    </Button>
  );
};

export default NormButton;