import React from 'react';
import { Container, Grid } from '@material-ui/core';
import { Facebook, Instagram, YouTube, } from '@material-ui/icons';
import { CustPink } from '../settings/colors';
import NormButton from './NormButton';

const Footer = () => {
  const windowWidth = window.innerWidth;
  const iconStyle = {fontSize: windowWidth < 720 ? 35 : 45, color: CustPink}

  const goSocial = (opt) => {
    let socialNet = "";
    switch(opt) {
      case 0:
        socialNet = "https://www.facebook.com/NigoGamesLatam/"
      break;
      case 1:
        socialNet = "https://www.instagram.com/nigo_games/"
      break;
      case 2:
        socialNet = "https://www.youtube.com/channel/UCHSEOo1biNLYkGpB7BFaEXA"
      break;
      default:
        socialNet = "https://www.facebook.com/NigoGamesLatam/"
      break;
    }
    window.open(socialNet, "_blank");
  }

  return (
    <Container className="footer__content" maxWidth="xs">
      <center><h1 className="footer__title">NIGO GAMES</h1></center>
      <center><p className="footer__subtitle">Copyright &copy; 2020 Nigo Games.</p></center>
      <Grid container={true} spacing={3} alignContent="space-around">
        <Grid item xs={4}>
          <center>
            <NormButton
              title={<Facebook style={iconStyle}/>}
              onClick={() => goSocial(0)}
            />
          </center>
        </Grid>
        <Grid item xs={4}>
          <center>
            <NormButton
              title={<Instagram style={iconStyle}/>}
              onClick={() => goSocial(1)}
            />
          </center>
        </Grid>
        <Grid item xs={4}>
          <center>
            <NormButton
              title={<YouTube style={iconStyle}/>}
              onClick={() => goSocial(2)}
            />
          </center>
        </Grid>
      </Grid>
    </Container>
  )
};

export default Footer;