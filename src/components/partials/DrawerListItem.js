import React from 'react';
import { ListItem, Icon } from '@material-ui/core';
import { CustPink } from '../settings/colors';

const DrawerListItem = (props) => {
  const drawerIcons = [
    'home',
    // 'emoji_events',
    // 'menu_book',
    'video_library',
    'contact_mail'
  ];

  return (
    <ListItem style={{marginTop: 12}} button onClick={props.clickListItem}>
      <Icon style={{fontSize: 20, paddingRight: 10, color: CustPink}}>{drawerIcons[props.index]}</Icon>
      <span className={props.index === props.active ? "list-item-text list-item-text--active" : "list-item-text"}>{props.text}</span>
    </ListItem>
  );
};

export default DrawerListItem;