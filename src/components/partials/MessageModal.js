import React from 'react';
import Modal from 'react-modal';
import FormButton from './FormButton';
import { CustPink } from '../settings/colors';

const MessageModal = (props) => {
  const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-40%',
      transform             : 'translate(-50%, -50%)'
    }
  };

  return (
    <Modal
      isOpen={props.open}
      contentLabel={props.title}
      ariaHideApp={false}
      onRequestClose={props.onClose}
      closeTimeoutMS={100}
      role="dialog"
      style={customStyles}
    >
      <h3 className="modal__title">{props.title}</h3>
      <p className="modal__body">{props.body}</p>
      <FormButton
        fullWidth = {true}
        variant = "contained"
        size = "large"
        title = "Ok"
        type = "button"
        onClick = {props.onClose}
        colorBtn = { CustPink }
        colorBtnTxt = '#fff'
      />
    </Modal>
  );
};

export default MessageModal;