const CustNigoLightBlue = '#3FC2EC';
const CustNigoLightRed = '#D8130D';
const CustNigoDarkRed = '#B00000';
const CustLightGrey = '#5C5C5C';

const CustLightRed = '#EE3124';
const CustLightRedTwo = '#F22727';
const CustDarkRed = '#AC281F';
const CustDarkRedTwo = '#D63031';
const CustPink = '#CF514A';
const CustGrey = '#5C5C5C';
const CustDarkGrey = '#333333';
const CustDark = '#222222';
const CustLightPurple = '#9079CB';
const CustDarkPurple = '#8B58DC';
const CustLightBlue = '#43B4DE';
const CustLightBlueTwo = '#4C90CF';
const CustOrange = '#F28322';
const CustTeal = '#5EBECC';

export {
  CustNigoLightBlue,
  CustNigoLightRed,
  CustNigoDarkRed,
  CustLightGrey,
  CustDarkGrey,
  CustLightBlue,
  CustLightRed,
  CustDarkRed,
  CustDarkRedTwo,
  CustDark,
  CustLightRedTwo,
  CustPink,
  CustGrey,
  CustLightPurple,
  CustDarkPurple,
  CustLightBlueTwo,
  CustOrange,
  CustTeal
};