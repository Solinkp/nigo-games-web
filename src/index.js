import React from 'react';
import ReactDOM from 'react-dom';
import './styles/fonts';
import './styles/style.scss';
import AppRouter from './routers/AppRouter';
import Footer from './components/partials/Footer';

ReactDOM.render(<AppRouter />, document.getElementById('root'));
ReactDOM.render(<Footer />, document.getElementById('footer'));
