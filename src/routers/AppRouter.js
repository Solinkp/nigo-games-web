import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from '../components/partials/header/Header';
import Home from '../components/pages/Home';
// import Tournaments from '../components/pages/Tournaments';
// import News from '../components/pages/News';
import Contact from '../components/pages/Contact';
import Videos from '../components/pages/Videos';

const DefaultComp = () => (
  <div><center><h1>Page Not Found</h1></center></div>
);

const AppRouter = () => (
  <BrowserRouter>
    <div className="main-container">
      <Header />
      <div className="body-container">
        <Switch>
          <Route exact path='/' component={Home} />

          {/* <Route path='/login' component={DefaultComp} /> */}
          {/* <Route path='/signin' component={DefaultComp} /> */}

          {/* <Route path='/tournaments' component={Tournaments} /> */}
          {/* <Route path='/news' component={News} /> */}
          <Route path='/contact' component={Contact} />
          <Route path='/videos' component={Videos} />

          {/* ADMIN ROUTES */}
          {/* <Route path='/admin/dashboard' component={DefaultComp} /> */}
          {/* <Route path='/admin/news' component={DefaultComp} /> */}
          {/* <Route path='/admin/news-form' component={DefaultComp} /> for new and edit */}
          {/* <Route path='/admin/tournaments' component={DefaultComp} /> */}
          {/* <Route path='/admin/tournament-form' component={DefaultComp} /> for new and edit */}

          {/* PLAYER ROUTES */}
          {/* <Route path='/profile' component={DefaultComp} /> for new and edit */}

          <Route component={DefaultComp} />
        </Switch>
      </div>
    </div>
  </BrowserRouter>
);

export default AppRouter;